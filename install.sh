#!/bin/bash

hostnamectl set-hostname super.rocket.com

apt-get update -y

apt-get upgrade -y

apt-get install apache2 apache2-utils mariadb-server mariadb-client php7.4 libapache2-mod-php7.4 php7.4-mysql php-net-ldap2 php-net-ldap3 php-imagick php7.4-common php7.4-gd php7.4-imap php7.4-json php7.4-curl php7.4-zip php7.4-xml php7.4-mbstring php7.4-bz2 php7.4-intl php7.4-gmp php-net-smtp php-mail-mime php-net-idna2 mailutils wget -y

apt-get -y install lsb-release apt-transport-https ca-certificates 
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
apt-get update
apt-get install apache2 apache2-utils mariadb-server mariadb-client php7.4 libapache2-mod-php7.4 php7.4-mysql php-net-ldap2 php-net-ldap3 php-imagick php7.4-common php7.4-gd php7.4-imap php7.4-json php7.4-curl php7.4-zip php7.4-xml php7.4-mbstring php7.4-bz2 php7.4-intl php7.4-gmp php-net-smtp php-mail-mime php-net-idna2 mailutils

apt-get install postfix

# Verificamos la versión
postconf mail_version

# verificamos el estado
systemctl status postfix


apt-get install dovecot-imapd dovecot-pop3d

systemctl restart dovecot

wget https://github.com/roundcube/roundcubemail/releases/download/1.4.8/roundcubemail-1.4.8.tar.gz

tar -xvf roundcubemail-1.4.8.tar.gz
mv roundcubemail-1.4.8 /var/www/html/roundcubemail

chown -R www-data:www-data /var/www/html/roundcubemail/
chmod 755 -R /var/www/html/roundcubemail/

mysql -u root < prepare_roundcube.sql
mysql roundcube < /var/www/html/roundcubemail/SQL/mysql.initial.sql
mv roundcube.conf /etc/apache2/sites-available/roundcube.conf

a2ensite roundcube.conf
systemctl reload apache2

