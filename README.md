# Redes-ProyectoFinal

Proyecto Final: Servidor de correo

## Description
Servidor de correo
Implementar un servidor de correo electrónico mediante el uso de SMTP con las siguientes
características:
● Se debe hacer uso de Postfix para la implementación del servidor de correo
Este tema se abordará en un video próximamente
● Implementar el protocolo IMAP para acceder al correo, haciendo uso de Dovecot.
Para desarrollar esta parte del proyecto, deberán llevar a cabo la investigación y
documentación correspondiente
● Instalar un servicio Webmail con SquirrelMail o RoundCube
● Como parte de la entrega, se deberá crear una cuenta de correo para cada uno de los
profesores del curso para poder llevar a cabo la revisión del servicio mediante el uso de
un cliente. Las credenciales correspondientes a estas cuentas se deberán entregar junto
con la documentación del proyecto.
En el siguiente diagrama se muestra el funcionamiento de este servicio:

## Authors and acknowledgment
- Andrea Regina García Correa 
- Brayan Martínez Santana
- Luis Alberto Hérnandez Aguilar 
- Miguel Angel Torres Sánchez
